 <?php get_header(); ?>
 
 <!--Main Banner-->
    <div class="banner_wapper wow fadeIn">
 		<div class="homebanner">
 			<?php 
 				$slider = 1;
 				$args = array('post_type' => 'home_slider');
 				$loop = new WP_Query($args);
 				while ($loop->have_posts()) : $loop->the_post();
 				$slider = get_the_ID();
 			?>
	        	<div class="item <?php if($slider == 1){echo 'active'; }?>">
	        		<?php /*<img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="">*/ ?>
	            	<?php the_post_thumbnail();?>
	                <div class="container cf">
	                	<div class="slider-text">
	                		<h1><?php the_title(); ?></h1>
	                		<h4><?php the_excerpt(); ?></h4>
	                		<a href="#" class="green-btn">Read More</a>
	                     </div>
	                </div>
	            </div>
	          	<?php
	            	$slider++;
	            	endwhile;
	            ?>
        </div>
    </div>
    <!--End Main Banner-->
    
    <!-- Welcome To DPS -->
    <section class="section welcome-dps">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-7 wow fadeInLeft">
                	<div class="title-big"><h2>Welcome to DPS Rajkot</h2></div>
                    <p>Rajkot has emerged as a fast growing industrial hub in Gujarat and attained eminence as a centre of excellence in higher education in the Saurashtra region.</p>
					<p>Delhi Public School Rajkot founded in 2002, is one of the schools run under the aegis of Delhi Public School Society, recognized throughout the academic world for its progressive approach to education, path breaking educational practices and commitment to excellence.</p>
					<p>The school campus is located on the outskirts of the city at Haripar, away from the noise and congestion characteristic of the city life, in lush green and pollution free surroundings.</p>
                    <div class="welcome-icons">
                    	<ul>
                        	<li class="wow fadeIn" data-wow-delay="1s"><span><i class="fa fa-handshake-o"></i></span> Good Planning</li>
                            <li class="wow fadeIn" data-wow-delay="2s"><span><i class="fa fa-book"></i></span> Our Courses</li>
                            <li class="wow fadeIn" data-wow-delay="3s"><span><i class="fa fa-smile-o"></i></span> Happy Students</li>
                            <li class="wow fadeIn" data-wow-delay="4s"><span><i class="fa fa fa-users"></i></span> Our Teachers</li>
                            <li class="wow fadeIn" data-wow-delay="5s"><span><i class="fa fa-check-square-o"></i></span> Special for you</li>
                            <li class="wow fadeIn" data-wow-delay="6s"><span><i class="fa fa-life-bouy"></i></span> Help tools</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-5 wow fadeInRight">
                	<div class="welcome-img"><img src="<?php echo get_template_directory_uri(); ?>/images/welcome-img.jpg" alt=""></div>
                </div>
            </div>
        </div>
    </section>    
    
    <!-- Event Gallery -->
    <section class="section event-gallery " style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/parallax-banner1.jpg);">
    	<div class="container">
        	<div class="title-big wow fadeInDown"><h2>Event Gallery</h2></div>
            <div class="event-gallery-slider wow fadeIn">
            	<div class="item">
                	<img src="<?php echo get_template_directory_uri(); ?>/images/eg-1.jpg" alt="">
                    <h3><a href="#">Introduction LearnPress</a></h3>
                    <p>We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
                    <a href="#" class="green-btn">Read More</a>
                </div>
                <div class="item">
                	<img src="<?php echo get_template_directory_uri(); ?>/images/eg-2.jpg" alt="">
                    <h3><a href="#">Introduction LearnPress</a></h3>
                    <p>We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
                    <a href="#" class="green-btn">Read More</a>
                </div>
                <div class="item">
                	<img src="<?php echo get_template_directory_uri(); ?>/images/eg-3.jpg" alt="">
                    <h3><a href="#">Introduction LearnPress</a></h3>
                    <p>We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
                    <a href="#" class="green-btn">Read More</a>
                </div>
            </div>
        </div>
    </section>
    
    <!-- Latest Update -->
    <section class="section latest-update">
    	<div class="container">
        	<div class="title-big wow fadeInDown"><h2>Latest Updates</h2></div>
        	<div class="row wow fadeIn">
            	<div class="col-md-4">
                	<img src="<?php echo get_template_directory_uri(); ?>/images/lu-1.jpg" alt="">
                    <h3><a href="#">Talent Show of Class 3</a></h3>
                    <p>We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
                    <a href="#" class="green-btn">Read More</a>
                </div>
                <div class="col-md-4">
                	<img src="<?php echo get_template_directory_uri(); ?>/images/lu-2.jpg" alt="">
                    <h3><a href="#">Talent Show of Class 3</a></h3>
                    <p>We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
                    <a href="#" class="green-btn">Read More</a>
                </div>
                <div class="col-md-4">
                	<img src="<?php echo get_template_directory_uri(); ?>/images/lu-3.jpg" alt="">
                    <h3><a href="#">Talent Show of Class 3</a></h3>
                    <p>We offer the most complete house renovating services in the country, from kitchen design to bathroom remodeling.</p>
                    <a href="#" class="green-btn">Read More</a>
                </div>
            </div>
        </div>
    </section>
    
    <!-- School Details -->
    <section class="school-details">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-4 wow fadeIn" data-wow-delay="1s">
                	<div class="sd-block text-center">
                    	<h3>Affiliation No:</h3>
                        <h2>430054</h2>
                    </div>
                </div>
                <div class="col-md-4 wow fadeIn" data-wow-delay="2s">
                	<div class="sd-block text-center">
                    	<h3>School Code:</h3>
                        <h2>13015 </h2>
                    </div>
                </div>
                <div class="col-md-4 wow fadeIn" data-wow-delay="3s">
                	<div class="sd-block text-center">
                    	<h3>School DISE Number:</h3>
                        <h2>24090701105 </h2>
                    </div>
                </div>
            </div>
        </div> 
    </section>
    
    <!-- Reviews -->
    <section class="section reviews-block">
    	<div class="container">
        	<div class="title-big text-center wow fadeInDown"><h2>Reviews</h2></div>
            <div class="col-md-8 center-block float_none wow fadeInLeft">	
                <div class="reviews-slider text-center">
                    <div class="item">
                        <div class="user-pic"></div>
                        <div class="review-quote"><img src="<?php echo get_template_directory_uri(); ?>/images/quote.png" alt=""></div>
                        <h3>Keyur Borad</h3>
                        <h4>Students (Batch 2012-2013)</h4>
                        <p>I took admission in DPS when I was in Class 8. At that time, ?I was quite apprehensive about the school,. Whether I would make friends, Whether I would be able to adjust to the surroundings, and the list was long………. However even before I realized, I became an integral part of the school, adjusting quite easily. The overwhelming ambience of the school, the enormous sports ground and above all the numerous trees that seemed like a green envelope around the huge cement building- became part and parcel of my life! I was always in awe of the school throughout the 5 years that I was there.</p>
                    </div>
                    <div class="item">
                        <div class="user-pic"></div>
                        <div class="review-quote"><img src="<?php echo get_template_directory_uri(); ?>/images/quote.png" alt=""></div>
                        <h3>Keyur Borad</h3>
                        <h4>Students (Batch 2012-2013)</h4>
                        <p>I took admission in DPS when I was in Class 8. At that time, ?I was quite apprehensive about the school,. Whether I would make friends, Whether I would be able to adjust to the surroundings, and the list was long………. However even before I realized, I became an integral part of the school, adjusting quite easily. The overwhelming ambience of the school, the enormous sports ground and above all the numerous trees that seemed like a green envelope around the huge cement building- became part and parcel of my life! I was always in awe of the school throughout the 5 years that I was there.</p>
                    </div>
                    <div class="item">
                        <div class="user-pic"></div>
                        <div class="review-quote"><img src="<?php echo get_template_directory_uri(); ?>/images/quote.png" alt=""></div>
                        <h3>Keyur Borad</h3>
                        <h4>Students (Batch 2012-2013)</h4>
                        <p>I took admission in DPS when I was in Class 8. At that time, ?I was quite apprehensive about the school,. Whether I would make friends, Whether I would be able to adjust to the surroundings, and the list was long………. However even before I realized, I became an integral part of the school, adjusting quite easily. The overwhelming ambience of the school, the enormous sports ground and above all the numerous trees that seemed like a green envelope around the huge cement building- became part and parcel of my life! I was always in awe of the school throughout the 5 years that I was there.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
<?php get_footer(); ?>
