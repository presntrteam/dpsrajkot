
// Browser Selecter
function css_browser_selector(e){var r=e.toLowerCase(),o=function(e){return r.indexOf(e)>-1},i="gecko",n="webkit",t="safari",s="opera",f="mobile",a=document.documentElement,d=[!/opera|webtv/i.test(r)&&/msie\s(\d)/.test(r)?"ie ie"+RegExp.$1:o("firefox/2")?i+" ff2":o("firefox/3.5")?i+" ff3 ff3_5":o("firefox/3.6")?i+" ff3 ff3_6":o("firefox/3")?i+" ff3":o("gecko/")?i:o("opera")?s+(/version\/(\d+)/.test(r)?" "+s+RegExp.$1:/opera(\s|\/)(\d+)/.test(r)?" "+s+RegExp.$2:""):o("konqueror")?"konqueror":o("blackberry")?f+" blackberry":o("android")?f+" android":o("chrome")?n+" chrome":o("iron")?n+" iron":o("applewebkit/")?n+" "+t+(/version\/(\d+)/.test(r)?" "+t+RegExp.$1:""):o("mozilla/")?i:"",o("j2me")?f+" j2me":o("iphone")?f+" iphone":o("ipod")?f+" ipod":o("ipad")?f+" ipad":o("mac")?"mac":o("darwin")?"mac":o("webtv")?"webtv":o("win")?"win"+(o("windows nt 6.0")?" vista":""):o("freebsd")?"freebsd":o("x11")||o("linux")?"linux":"","js"];return c=d.join(" "),a.className+=" "+c,c}css_browser_selector(navigator.userAgent);


// Animation On scroll
(function(){var t,e=function(t,e){return function(){return t.apply(e,arguments)}};t=function(){function t(){}return t.prototype.extend=function(t,e){var i,n;for(i in t)n=t[i],null!=n&&(e[i]=n);return e},t.prototype.isMobile=function(t){return/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(t)},t}(),this.WOW=function(){function i(t){null==t&&(t={}),this.scrollCallback=e(this.scrollCallback,this),this.scrollHandler=e(this.scrollHandler,this),this.start=e(this.start,this),this.scrolled=!0,this.config=this.util().extend(t,this.defaults)}return i.prototype.defaults={boxClass:"wow",animateClass:"animated",offset:0,mobile:!0},i.prototype.init=function(){var t;return this.element=window.document.documentElement,"interactive"===(t=document.readyState)||"complete"===t?this.start():document.addEventListener("DOMContentLoaded",this.start)},i.prototype.start=function(){var t,e,i,n;if(this.boxes=this.element.getElementsByClassName(this.config.boxClass),this.boxes.length){if(this.disabled())return this.resetStyle();for(n=this.boxes,e=0,i=n.length;i>e;e++)t=n[e],this.applyStyle(t,!0);return window.addEventListener("scroll",this.scrollHandler,!1),window.addEventListener("resize",this.scrollHandler,!1),this.interval=setInterval(this.scrollCallback,50)}},i.prototype.stop=function(){return window.removeEventListener("scroll",this.scrollHandler,!1),window.removeEventListener("resize",this.scrollHandler,!1),null!=this.interval?clearInterval(this.interval):void 0},i.prototype.show=function(t){return this.applyStyle(t),t.className=""+t.className+" "+this.config.animateClass},i.prototype.applyStyle=function(t,e){var i,n,o;return n=t.getAttribute("data-wow-duration"),i=t.getAttribute("data-wow-delay"),o=t.getAttribute("data-wow-iteration"),t.setAttribute("style",this.customStyle(e,n,i,o))},i.prototype.resetStyle=function(){var t,e,i,n,o;for(n=this.boxes,o=[],e=0,i=n.length;i>e;e++)t=n[e],o.push(t.setAttribute("style","visibility: visible;"));return o},i.prototype.customStyle=function(t,e,i,n){var o;return o=t?"visibility: hidden; -webkit-animation-name: none; -moz-animation-name: none; animation-name: none;":"visibility: visible;",e&&(o+="-webkit-animation-duration: "+e+"; -moz-animation-duration: "+e+"; animation-duration: "+e+";"),i&&(o+="-webkit-animation-delay: "+i+"; -moz-animation-delay: "+i+"; animation-delay: "+i+";"),n&&(o+="-webkit-animation-iteration-count: "+n+"; -moz-animation-iteration-count: "+n+"; animation-iteration-count: "+n+";"),o},i.prototype.scrollHandler=function(){return this.scrolled=!0},i.prototype.scrollCallback=function(){var t;return this.scrolled&&(this.scrolled=!1,this.boxes=function(){var e,i,n,o;for(n=this.boxes,o=[],e=0,i=n.length;i>e;e++)t=n[e],t&&(this.isVisible(t)?this.show(t):o.push(t));return o}.call(this),!this.boxes.length)?this.stop():void 0},i.prototype.offsetTop=function(t){var e;for(e=t.offsetTop;t=t.offsetParent;)e+=t.offsetTop;return e},i.prototype.isVisible=function(t){var e,i,n,o,s;return i=t.getAttribute("data-wow-offset")||this.config.offset,s=window.pageYOffset,o=s+this.element.clientHeight-i,n=this.offsetTop(t),e=n+t.clientHeight,o>=n&&e>=s},i.prototype.util=function(){return this._util||(this._util=new t)},i.prototype.disabled=function(){return!this.config.mobile&&this.util().isMobile(navigator.userAgent)},i}()}).call(this),wow=new WOW({animateClass:"animated",offset:100}),wow.init();


// CSS Menu
!function(s){s.fn.menumaker=function(n){var i=s(this),e=s.extend({title:"Menu",format:"dropdown",sticky:!1},n);return this.each(function(){return i.prepend('<div id="menu-button">'+e.title+"</div>"),s(this).find("#menu-button").on("click",function(){s(this).toggleClass("menu-opened");var n=s(this).next("ul");n.hasClass("open")?n.hide().removeClass("open"):(n.show().addClass("open"),"dropdown"===e.format&&n.find("ul").show())}),i.find("li ul").parent().addClass("has-sub"),multiTg=function(){i.find(".has-sub").prepend('<span class="submenu-button"></span>'),i.find(".submenu-button").on("click",function(){s(this).toggleClass("submenu-opened"),s(this).siblings("ul").hasClass("open")?s(this).siblings("ul").removeClass("open").hide():s(this).siblings("ul").addClass("open").show()})},"multitoggle"===e.format?multiTg():i.addClass("dropdown"),e.sticky===!0&&i.css("position","fixed"),resizeFix=function(){s(window).width()>991&&i.find("ul").show(),s(window).width()<=991&&i.find("ul").hide().removeClass("open")},resizeFix(),s(window).on("resize",resizeFix)})}}(jQuery),function(s){s(document).ready(function(){s(document).ready(function(){s("#cssmenu").menumaker({title:"Menu",format:"multitoggle"}),s("#cssmenu").prepend("<div id='menu-line'></div>");var n,i,e,t,o=!1,u=0,d=s("#cssmenu #menu-line");s("#cssmenu > ul > li").each(function(){s(this).hasClass("active")&&(n=s(this),o=!0)}),o===!1&&(n=s("#cssmenu > ul > li").first()),t=i=n.width(),e=u=n.position().left,d.css("width",i),d.css("left",u),s("#cssmenu > ul > li").hover(function(){n=s(this),i=n.width(),u=n.position().left,d.css("width",i),d.css("left",u)},function(){d.css("left",e),d.css("width",t)})})})}(jQuery);





jQuery(document).ready( function($) {
 
 	// Bootstrap Select
	$('.selectpicker').selectpicker();
 
 	// Home Banner
	$('.homebanner').owlCarousel({
		loop:true,
		margin:0,
		dots:false,
		nav:true,
		smartSpeed:1500,
		autoplay:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:1
			}
		},
		afterUpdate: function () {
            updateSize();
        },
        afterInit:function(){
            updateSize();
        },
	})
	
	// Event Gallery
	$('.event-gallery-slider').owlCarousel({
		loop:true,
		margin:30,
		dots:false,
		nav:true,
		smartSpeed:1500,
		autoplay:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	})
	
	// Reviews
	$('.reviews-slider').owlCarousel({
		//loop:true,
		margin:0,
		dots:true,
		nav:false,
		smartSpeed:1500,
		autoplay:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:1
			}
		}
	})
	
})



