<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	<link href='<?php echo get_template_directory_uri();?>/simplelightbox-master/dist/simplelightbox.min.css' rel='stylesheet' type='text/css'>
	<link href='<?php echo get_template_directory_uri();?>/simplelightbox-master/demo/demo.css' rel='stylesheet' type='text/css'>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( ! is_page_template( 'page-templates/front-page.php' ) ) : ?>
			<?php the_post_thumbnail(); ?>
		<?php endif; ?>
		<div class="title-big"><h2><?php the_title(); ?></h2></div>
		<p>
			<?php the_content(); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'twentytwelve' ), 'after' => '</div>' ) ); ?>
		</p>		
		<?php 
			$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1));
			$post_id = get_the_ID();
			$images = get_post_meta($post_id,'page_images',true);
			$images = trim($images);
			if($images != '0' && $images != ''){
		?>
			<div class="container" style=" margin-top:30px; padding: 20px; border-radius: 5px; ">
				<div class="gallery">
					<?php
						for($i=0; $i < $images; $i++){
							echo $image_title = get_post_meta($post_id,'page_images_'.$i.'_image_title',true);
							$image_id = get_post_meta($post_id,'page_images_'.$i.'_image_name',true);
							$image_thumb = wp_get_attachment_image_src($image_id,'medium');
							$image_link = wp_get_attachment_url( $image_id ); 
					?>
							<a href="<?php echo esc_url($image_link); ?>"><img src="<?php echo $image_thumb[0]; ?>" ></a>
					<?php } ?>
				</div>
			</div>
		<?php } ?>
		<footer class="entry-meta">
			<?php //edit_post_link( __( 'Edit', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?>
		</footer><!-- .entry-meta -->
	</article><!-- #post -->
	<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/simplelightbox-master/dist/simple-lightbox.js"></script>
	<script>
		(function($) {
			var $gallery = jQuery('.gallery a').simpleLightbox();

			$gallery.on('show.simplelightbox', function(){
				console.log('Requested for showing');
			})
			.on('shown.simplelightbox', function(){
				console.log('Shown');
			})
			.on('close.simplelightbox', function(){
				console.log('Requested for closing');
			})
			.on('closed.simplelightbox', function(){
				console.log('Closed');
			})
			.on('change.simplelightbox', function(){
				console.log('Requested for change');
			})
			.on('next.simplelightbox', function(){
				console.log('Requested for next');
			})
			.on('prev.simplelightbox', function(){
				console.log('Requested for prev');
			})
			.on('nextImageLoaded.simplelightbox', function(){
				console.log('Next image loaded');
			})
			.on('prevImageLoaded.simplelightbox', function(){
				console.log('Prev image loaded');
			})
			.on('changed.simplelightbox', function(){
				console.log('Image changed');
			})
			.on('nextDone.simplelightbox', function(){
				console.log('Image changed to next');
			})
			.on('prevDone.simplelightbox', function(){
				console.log('Image changed to prev');
			})
			.on('error.simplelightbox', function(e){
				console.log('No image found, go to the next/prev');
				console.log(e);
			});
		})(jQuery);
	</script>