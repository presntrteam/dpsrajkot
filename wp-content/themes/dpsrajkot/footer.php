<!--Main Footer-->
    <footer id="footer">
        <div class="footer_wapper">
        	<div class="container">
            	<div class="row">
                	<div class="col-sm-6 col-md-3">
                    	<?php
                        if(is_active_sidebar('footer_1')){
                            dynamic_sidebar('footer_1');
                        }       
                        ?>
                    </div>
                    <div class="col-sm-6 col-md-3">
						<?php
							if(is_active_sidebar('footer_2')){
								dynamic_sidebar('footer_2');
							}       
						?>
                    </div>
                    <div class="col-sm-6 col-md-3">
                    	<div class="footer-col footer-gallery">
                            <h3>Our Video Gallery</h3>
							<?php echo do_shortcode('[VDGAL id=1188]');?>
						</div>
                    </div>
                    <div class="col-sm-6 col-md-3">
						<div class="footer-col">
                        	<h3>Newsletter</h3>
                                <li id="nav_menu-3" class="widget widget_nav_menu"><div class="menu-newsletter-container"><ul id="menu-newsletter" class="menu"><li id="menu-item-863" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-863"><a href="<?php echo esc_url(home_url('/newsletter'));?>">Newsletters</a></li>
                                </ul></div></li>
                        </div>
                    </div>
                </div>
                
                <div class="footer-bottom">
                	<div class="copyright">© 2017 All Rights Reserved.</div>
                    <?php wp_nav_menu( array( 'theme_location' => 'footer', 'container' => 'ul', 'menu_class' => '' ) ); ?>
                </div>
                
            </div>
        </div>
    </footer>
    <!--End Main Footer-->
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>

<!-- owl js start -->
<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.js"></script>
<!-- owl js End -->

<!--Custom JS-->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-select.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<?php wp_footer(); ?>
</body>
</html>    
