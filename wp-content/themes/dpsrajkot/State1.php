<?php
/*
	Template Name: State1
*/
?>
<?php get_header(); ?>
<?php get_sidebar(); ?>
<?php
    
    function custom_taxonomies_terms_links(){
        global $post, $post_id;
        $post = &get_post($post->ID);
        $post_type = $post->post_type;
        $taxonomies = get_object_taxonomies($post_type);
        $out = "<ul>";
        foreach ($taxonomies as $taxonomy) {
            $out .= "<li>".$taxonomy.": ";
            $terms = get_the_terms($post->ID, $taxonomy);
            if(!empty($terms)){
                foreach ($terms as $term) 
                    $out .= '<a href="' .get_term_link($term->slug, $taxonomy) .'">'.$term->name.'</a>';
                
            }
            $out .="</li>";
        }
        $out .="</ul>";
        return $out;
    }
?>

<?php get_footer(); ?>
