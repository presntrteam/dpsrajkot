<?php
/*
 *	Template Name: State
*/
?>
<?php get_header(); ?>
<?php get_sidebar(); ?>
<?php
//$args = array('taxonomy' => 'states', 'hide_empty' => FALSE);
//wp_list_categories($args);
?>
<div class="tools-dropdown">
	<form class="tool-category-select" id="tool-category-select" method="get">
		<?php
			//Create and display the drop down menu
			wp_dropdown_categories(array(
				'orderby' 			=> 'NAME',
				'taxonomy' 			=> 'states',
				'name' 				=> 'state',
				'hide_empty' => FALSE,
				'show_option_all'	=> 'All States',
				'selected'			=> pn_get_selected_taxonomy_dropdown_term(),
			));
		?>
		
		<!--<input type="submit" value="View">-->
	</form>
</div>

<section id="tools-listing">
	<?php $state_in_taxonomy_term = pn_get_states_in_taxonomy_term();?>
	<?php if($state_in_taxonomy_term->the_post() ) :?>
	<?php while ($state_in_taxonomy_term->have_posts() ) : $state_in_taxonomy_term->the_post(); ?>
	
	<article class="state-entry">
		<h1><?php the_title();?> </h1>
		<p><?php the_content(); ?></p>
	</article>
	
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
	<?php endif; ?>
</section>

<!--Add Post tags-->

<?php
if( 'POST' == $_SERVER['REQUEST_METHOD']) {

    $title =  $_POST['title'];
    $description = $_POST['description'];
    $tags = $_POST['post_tags'];

    $new_post = array(
    'post_title'    =>  $title,
    'post_content'  =>  $description,
    'post_category' =>  array($_POST['cat']),  // Usable for custom taxonomies too
    'tags_input'    =>  array($tags),
    'post_status'   =>  'publish',           // Choose: publish, preview, future, draft, etc.
    'post_type' =>  'states'  //'post',page' or use a custom post type if you want to
);

    //SAVE THE POST
    $pid = wp_insert_post($new_post);

    //SET OUR TAGS UP PROPERLY
    wp_set_post_tags($pid, $_POST['post_tags']);

    //POST THE POST
    do_action('wp_insert_post', 'wp_insert_post');

}
$taxonomies = array( 
    'states'
);
$args = array(
    'orderby' => 'name',
    'order' => 'ASC',
    'hide_empty' => false
);

$terms = get_terms($taxonomies,$args);

if (count($terms) > 0):
	i == 0;
    foreach ($terms as $term): ?>
        <div class="wissen_tag_list">
            <input type="radio" value="<?php echo $term->term_id; ?>" name="state" class="wissen_tag_list_ckb" <?php if ( $i == 0 ) { ?>checked<?php } ?>>
            <label class="wissen_tag_list_ckbl">
                <?php echo $term->name; ?>
            </label>
        </div>
<?php
    $i++; endforeach;
endif; ?>

<form id="new_post" name="new_post" method="post" action="" enctype="multipart/form-data">

    <?php wp_dropdown_categories( 'tab_index=10&taxonomy=category&hide_empty=0' ); ?>
    <textarea id="description"  name="description"></textarea>
    <textarea id="post_tags"  name="post_tags"></textarea>
    <input type="hidden" name="action" value="new_post" />
    <input type="submit" value="submit" id="submit" name="submit" />

</form>

<?php get_footer();?>




