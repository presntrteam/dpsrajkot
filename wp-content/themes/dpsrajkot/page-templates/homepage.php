<?php
/* Template Name: Home Page */
 get_header();
 $home_id = 44;
 ?>
 
 <!--Main Banner-->
    <div class="banner_wapper wow fadeIn">
 		<div class="homebanner">
 			<?php 
 				$slider = 1;
 				$args = array('post_type' => 'home_slider');
 				$loop = new WP_Query($args);
 				while ($loop->have_posts()) : $loop->the_post();
 				$slider = get_the_ID();
 			?>
	        	<div class="item <?php if($slider == 1){echo 'active'; }?>">
	        		<img src="<?php the_post_thumbnail_url(); ?>" alt="">
	            	<?php //the_post_thumbnail();?>
	            </div>
	          	<?php
	            	$slider++;
	            	endwhile;
	            ?>
        </div>
    </div>
    <!--End Main Banner-->
    
    <!-- Welcome To DPS -->
    <section class="section welcome-dps">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-7 wow fadeInLeft">
					<?php
						$post = get_post($home_id); //assuming $id has been initialized
						setup_postdata($post);
					?>
					<div class="title-big"><h2><?php echo get_post_meta($home_id,'welcome_title',true);?></h2></div>
                    <p><?php the_content();?></p>
                    <div class="welcome-icons">
                    	<ul>
                        	<li class="wow fadeIn" data-wow-delay="1s"><a href="<?php echo esc_url( home_url( '/resources/syllabus/' ) ); ?>"><span><i class="fa fa-handshake-o"></i></span>Curriculum</a></li>
                            <li class="wow fadeIn" data-wow-delay="2s"><a href="<?php echo esc_url( home_url( '/students-council' ) ); ?>"><span><i class="fa fa-book"></i></span>Student's Council</a></li>
                            <li class="wow fadeIn" data-wow-delay="3s"><a href="<?php echo esc_url( home_url( '/circulars' ) ); ?>"><span><i class="fa fa-smile-o"></i></span>Circulars</a></li>
							<li class="wow fadeIn" data-wow-delay="4s"><a href="<?php  echo esc_url( home_url( '/media-coverage' ) );?>"><span><i class="fa fa-check-square-o"></i></span>Media Coverage</a></li>
                            <li class="wow fadeIn" data-wow-delay="5s">
                            <a href="<?php  echo esc_url( home_url( '/alumni' ) );?>">
                            <span><i class="fa fa-life-bouy"></i></span>Alumni</a></li>
							<li class="wow fadeIn" data-wow-delay="6s"><a href="<?php echo esc_url( home_url( '/vacancies' ) ); ?>"><span><i class="fa fa fa-users"></i></span>Vacancy</a></li>
                        </ul>
                    </div>
					
                </div>
                <div class="col-md-5 wow fadeInRight">
                	<div class="welcome-img"><?php the_post_thumbnail();?></div>
                </div>
            </div>
        </div>
    </section>    
    
    <!-- Event Gallery -->
	<section class="section event-gallery " style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/parallax-banner1.jpg);">
    	<div class="container">
        	<div class="title-big wow fadeInDown"><h2>Event Gallery</h2></div>
            <div class="event-gallery-slider wow fadeIn">
				<?php echo do_shortcode('[cws_gpp_albums results_page="image-gallery-3" theme=grid]'); ?>
            </div>
			<a href="<?php echo esc_url( home_url( '/all-event-images' ) ); ?>" class="green-btn pull-right">View All</a>
        </div>
    </section>
    
    <!-- Latest Update -->
    <section class="section latest-update">
    	<div class="container">
        	<div class="row title-big wow fadeInDown">
				<div class="col-md-8">
					<h2>Latest Updates</h2>
				</div>
				<div class="col-md-4">
					<h2>Virtual Video</h2>
				</div>
			</div>
        	<div class="row wow fadeIn">
				<div class="col-md-8" style="overflow: scroll;height: 300px;overflow-x: hidden;border: 1px solid #999; margin-top: 17px; padding-top: 20px;">
					<ul>
						<li><a href="">NASA USA TOUR</a></li>
						&nbsp;
						<li><a href="">IIMUN Rajkot</a></li>
						&nbsp;
						<li><a href="">Trip schedule class xI &amp; XII</a></li>
						&nbsp;
						<li><a href="">Trip schedule class Vi &amp; VIII</a></li>
						&nbsp;
						<li><a href="">School DISE Number</a></li>
						&nbsp;
						<li><a href="">Dubai Visa Documents</a></li>
						&nbsp;
						<li><a href="">Dubai Program</a></li>
						&nbsp;
						<li><a href="">Mission Statement</a></li>
						&nbsp;
						<li><a href="">New Telephone Numbers</a></li>
						&nbsp;
						<li><a href="">Extra Classes</a></li>
						&nbsp;
						<li><a href="">ASSESSMENT TEST SYLLABUS</a></li>
						&nbsp;
						<li><a href="">Links of Youtube Live Streaming of Journey (Part-1 and 2)</a></li>
						&nbsp;
						<li><a href="">Launch of ‘Saransh Portal - http://saransh.nic.in ’ and mobile application</a></li>
						&nbsp;
						<li><a href="">CBSE Notification -Collection of Online Feedback</a></li>
						&nbsp;
						<li><a href="">Mr. Manoj Dubey, Principal DPS Rajkot being honoured with CBSE Award 2014 by Smt. Smriti Irani, Hon’ble HRD Minister, Government of India on 3rd September 2015 at New Delhi.</a></li>
						&nbsp;
						<li><a href="">Prime Ministers Relief Fund</a></li>
					</ul>
				</div>
				<div class="col-md-4">
					<?php
						global $post;
						$args = array('numberposts' => 3,'category_name' => 'virtual-videos' );
						$posts = get_posts( $args );
						foreach( $posts as $post ): setup_postdata($post); 
						//echo "hi";exit;
					?>
					<div>
						<p><?php the_content(); ?></p>
						<h3><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h3>
						<a href="<?php the_permalink(); ?>" class="green-btn">Read More</a>
					</div>
					<?php
						wp_reset_postdata();
					?>
					<?php
						endforeach;
					?>
				</div>
			</div>
        </div>
    </section>

    
    <!-- School Details -->
    <section class="school-details">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-4 wow fadeIn" data-wow-delay="1s">
                	<div class="sd-block text-center">
                    	<h3>Affiliation No:</h3>
                        <h2>430054</h2>
                    </div>
                </div>
                <div class="col-md-4 wow fadeIn" data-wow-delay="2s">
                	<div class="sd-block text-center">
                    	<h3>School Code:</h3>
                        <h2>13015 </h2>
                    </div>
                </div>
                <div class="col-md-4 wow fadeIn" data-wow-delay="3s">
                	<div class="sd-block text-center">
                    	<h3>School DISE Number:</h3>
                        <h2>24090701105 </h2>
                    </div>
                </div>
            </div>
        </div> 
    </section>
    
    <!-- Reviews -->
    <section class="section reviews-block">
    	<div class="container">
        	<div class="title-big text-center wow fadeInDown"><h2>Reviews</h2></div>
            <div class="col-md-8 center-block float_none wow fadeInLeft">	
                <div class="reviews-slider text-center">
					<?php
						$args = array( 'post_type' => 'review' );
						$loop = new WP_Query( $args );
						while ( $loop->have_posts() ) : $loop->the_post();
					?>
							<div class="item">
								<div class="user-pic"><img class="img-circle" src="<?php echo get_the_post_thumbnail_url($post, 'post-thumbnail'); ?>" alt="Image"></div>
								<div class="review-quote"><img src="<?php echo get_template_directory_uri(); ?>/images/quote.png" alt=""></div>
								<h3><?php the_title(); ?></h3>
								<h4><?php echo get_post_meta( $post->ID, 'student_batch', true ); ?></h4>
								<p><?php the_content(); ?> </p>
							</div>
					<?php		
						endwhile;
					?>
                </div>
            </div>
        </div>
    </section>
    
<?php get_footer(); ?>
