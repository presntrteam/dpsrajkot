<?php
/* Template Name: Inquiry Form */
get_header();
$page_id = '209';
?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/parsleyjs/src/parsley.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap-datepicker.standalone.css">
<?php 
		
		if(!empty($_POST)){
			global $wpdb;
			$student_name = $_POST['student_name'];
			$date_of_birth = $_POST['date_of_birth'];
			$father_name = $_POST['father_name'];
			$father_profession = $_POST['father_profession'];
			$mother_name = $_POST['mother_name'];
			$mother_profession = $_POST['mother_profession'];
			$gender = $_POST['gender'];
			$place_of_birth = $_POST['place_of_birth'];
			$class_for_admission = $_POST['class_for_admission'];
			$residential_address = $_POST['residential_address'];
			$telephone_number = $_POST['telephone_number'];
			$email = $_POST['email'];
			$current_class = $_POST['current_class'];
			$medium_of_instruction = $_POST['medium_of_instruction'];
			$skills_intrest = $_POST['skills_intrest'];
			$transport_required = $_POST['transport_required'];
			
			$tablename='wp_inquiry_form';

			$data =array('student_name' => $student_name,'date_of_birth' => $date_of_birth,'father_name' => $father_name,'father_profession' => $father_profession,'mother_name' => $mother_name,'mother_profession' => $mother_profession,'gender' => $gender,'place_of_birth' => $place_of_birth,'class_for_admission' => $class_for_admission,'residential_address' => $residential_address,'telephone_number' => $telephone_number,'email' => $email,'current_class' => $current_class,'medium_of_instruction' => $medium_of_instruction,'skills_intrest' => $skills_intrest,'transport_required' => $transport_required);
				
			$success=$wpdb->insert( $tablename, $data);	
			
			/*Mail
			$headers = "From: $student_name <$email>\r\n";
			$email_message .= "Student's Name : ".$student_name."\n";
			$email_message .= "Date Of Birth : ".$date_of_birth."\n";
			$email_message .= "Father's Name : ".$father_name."\n";
			$email_message .= "Father's Profession : ".$father_profession."\n";
			$email_message .= "Mother's Name : ".$mother_name."\n";
			$email_message .= "Mother's Profession : ".$mother_profession."\n";
			$email_message .= "Gender : ".$gender."\n";
			$email_message .= "Place Of Birth (With Taluka & Dist.) : ".$place_of_birth."\n";
			$email_message .= "Class for which admission sought : ".$class_for_admission."\n";
			$email_message .= "Residential Address : ".$residential_address."\n";
			$email_message .= "Telephone No(s) : ".$telephone_number."\n";
			$email_message .= "Email : ".$email."\n";
			$email_message .= "Class in which the child is presently studying : ".$current_class."\n";
			$email_message .= "Medium of Instruction : ".$medium_of_instruction."\n";
			$email_message .= "Special skills and Interests : ".$skills_intrest."\n";
			$email_message .= "Transport Required : ".$transport_required."\n";
			
			wp_mail( 'presntr2@gmail.com', 'Inquiry From Website', $email_message, $headers);*/
		}
		
	?>
<div class="clearfix"></div>
	<section class="section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">

        			<div class="title-big"><h2><?php the_title(); ?></h2></div>

					<form class="form1" name="inq_form" id="form1" method="post" action="#" data-parsley-validate novalidate>
						<table width="700" border="0" cellspacing="0" cellpadding="0" class="border_01" align="center">
				          	<tbody>
				          		  <tr>
				                    <td><b class="font-style">Student’s  Name</b></td>
				                    <td><input type="text" id="student_name" name="student_name" class="triple-length" required></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Date  Of Birth</b></td>
				                    <td><input type="text" id="" name="date_of_birth" class="triple-length datepicker"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Father’s  Name</b></td>
				                    <td><input type="text" id="" name="father_name" class="triple-length"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Father’s  Profession</b></td>
				                    <td><input type="text" id="" name="father_profession" class="triple-length"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Mother’s  Name</b></td>
				                    <td><input type="text" id="" name="mother_name" class="triple-length"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Mother’s  Profession</b></td>
				                    <td><input type="text" id="" name="mother_profession" class="triple-length"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Gender</b></td>
				                    <td>
				                      <input name="gender" type="radio" value="M" class="chackbox01" checked=""> 
				                      <label>Male</label>
				                      <input name="gender" type="radio" value="F" class="chackbox01">
				                      <label>Female</label></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Place  Of Birth (With Taluka &amp; Dist.)</b></td>
				                    <td><input type="text" id="" name="place_of_birth" class="triple-length"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Class  for which admission sought</b></td>
				                    <td><input type="text" id="" name="class_for_admission" class="triple-length"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Residential  Address</b></td>
				                    <td><textarea id="residential_address" name="residential_address" class="triple-length doubleheight"></textarea></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Telephone  No(s</b>)</td>
				                    <td><input type="number" id="telephone_number" name="telephone_number" class="triple-length" required></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Email</b></td>
				                    <td><input type="email" id="email" name="email" class="triple-length" required></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Class  in which the child is presently studying</b></td>
				                    <td><input type="text" id="current_class" name="current_class" class="triple-length"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Medium  of Instruction</b></td>
				                    <td><input type="text" id="medium_of_instruction" name="medium_of_instruction" class="triple-length"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Special  skills and Interests</b></td>
				                    <td><input type="text" id="skills_intrest" name="skills_intrest" class="triple-length"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Transport  Required</b></td>
				                    <td><input type="text" id="transport_required" name="transport_required" class="triple-length"></td> 
				                  </tr>
				                  <tr>
				                    <td>&nbsp;</td>
				                    <td><input type="submit" name="Submit" value="Submit" class="button">
				                        <input type="reset" name="Reswe" value="Reset" class="button">
				                    </td>
				                  </tr>
				                  
				            </tbody>
				        </table>
					</form>
				
				</div>
			</div>
		</div>
	</section>
	<script src="<?php echo get_template_directory_uri();?>/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/parsleyjs/dist/parsley.min.js"></script>
	<script>
		(function(jQuery){
			jQuery(function(){
			jQuery('form').parsley();
			});
		})(jQuery);
		
		jQuery('.datepicker').datepicker({
			format: 'dd/mm/yyyy',
			orientation: 'auto bottom'
			
		});
		
	</script>
<?php get_footer(); ?>