<?php 
/*Template Name: Media Coverage*/
get_header();
?>
	<link href='<?php echo get_template_directory_uri();?>/simplelightbox-master/dist/simplelightbox.min.css' rel='stylesheet' type='text/css'>
	<link href='<?php echo get_template_directory_uri();?>/simplelightbox-master/demo/demo.css' rel='stylesheet' type='text/css'>
	<div class="clearfix"></div>
		<section class="section">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="title-big"><h2><?php the_title(); ?></h2></div>
					</div>
				</div>
				
				<?php 
					$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1));
					$post_id = get_the_ID();
					$images = get_post_meta($post_id,'media_coverage',true);
					$images = trim($images);
					if($images != '0' && $images != ''){
				?>
				<?php 
					$mc_year_arr = array();
					for ($t=0; $t < $images; $t++){
						$img_date = get_post_meta($post_id,'media_coverage_'.$t.'_date',true);
						$img_year = substr($img_date, -4);
						if(is_numeric($img_year)){
							if(in_array($img_year, $mc_year_arr, true)){
							} else {
								$mc_year_arr[] = $img_year;
							}
						}
					}
					$current_year = date('Y');
					if(isset($_POST['image_year'])){
						$current_year = $_POST['image_year'];
					}
				?>
				<form name="change_mc_year" id="change_mc_year" method="post" action="<?php echo get_permalink(); ?>">
					<label>Select Year</label>

					<select name="image_year" id="image_year" style="width: 15%; padding: 5px; border: 1px solid #999; background: transparent;">
						<?php foreach($mc_year_arr as $mc_year){ ?>
							<option value="<?php echo $mc_year; ?>" <?php if( $mc_year == $current_year ){ echo " Selected "; } ?> > <?php echo $mc_year; ?> </option>
						<?php } ?>
					</select>
				</form>
				<div class="container" style=" margin-top:30px; padding: 20px; border-radius: 5px; ">
					<div class="gallery media_coverage">
						<?php
							for($i=0; $i < $images; $i++){
								$image_display = get_post_meta($post_id,'media_coverage_'.$i.'_image',true);
								$image_title = get_post_meta($post_id,'media_coverage_'.$i.'_title',true);
								$image_date = get_post_meta($post_id,'media_coverage_'.$i.'_date',true);
								$image_thumb = wp_get_attachment_image_src($image_display,'thumbnail');
								$image_link = wp_get_attachment_url( $image_display ); 
								$image_year = substr($image_date, -4);
								if($image_year == $current_year){
						?>	
									<a href="<?php echo esc_url($image_link); ?>">
										<span><?php echo $image_title; ?></span><br />
										<img src="<?php echo $image_thumb[0]; ?>"><br />
										<span><?php echo $image_date; ?></span>
									</a>
						<?php 	}
							}
						?>
					</div>
				</div>
						
				<?php } ?>
			</div>
		</section>
		<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/simplelightbox-master/dist/simple-lightbox.js"></script>
		<script>
			(function($) {
				var $gallery = jQuery('.gallery a').simpleLightbox();

				$gallery.on('show.simplelightbox', function(){
					console.log('Requested for showing');
				})
				.on('shown.simplelightbox', function(){
					console.log('Shown');
				})
				.on('close.simplelightbox', function(){
					console.log('Requested for closing');
				})
				.on('closed.simplelightbox', function(){
					console.log('Closed');
				})
				.on('change.simplelightbox', function(){
					console.log('Requested for change');
				})
				.on('next.simplelightbox', function(){
					console.log('Requested for next');
				})
				.on('prev.simplelightbox', function(){
					console.log('Requested for prev');
				})
				.on('nextImageLoaded.simplelightbox', function(){
					console.log('Next image loaded');
				})
				.on('prevImageLoaded.simplelightbox', function(){
					console.log('Prev image loaded');
				})
				.on('changed.simplelightbox', function(){
					console.log('Image changed');
				})
				.on('nextDone.simplelightbox', function(){
					console.log('Image changed to next');
				})
				.on('prevDone.simplelightbox', function(){
					console.log('Image changed to prev');
				})
				.on('error.simplelightbox', function(e){
					console.log('No image found, go to the next/prev');
					console.log(e);
				});
			})(jQuery);
			jQuery(document).ready( function(){
				jQuery(document).on('change', '#image_year', function(){
					jQuery('#change_mc_year').submit();
				});
			});
		</script>	
<?php get_footer();?>