<?php
/* Template Name: Alumni  */
get_header();
//$page_id = '209';
?>
<?php
	global $wpdb;
	$alumni_result = $wpdb->get_results("SELECT * FROM wp_alumni_form;");
	//print_r($alumni_result);
?>
<div class="clearfix"></div>
	<section class="section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">
        			<div class="title-big"><h2><?php the_title(); ?></h2></div>
				</div>
				<div class="col-md-12">
        			<div class="col-md-2"><a href="<?php echo esc_url( home_url( '/alumni-form' ) ); ?>"><h4>Alumni Form</h4></a></div>
        			<div class="col-md-10"><h4>Latest updates by <a href="<?php echo esc_url( home_url( '/alumni-members' ) ); ?>">Alumni Members</a></h4></div>
				</div>
				<div class="col-md-12">
        			<div class="col-md-2"><h4>Batches :</h4></div>
        			<div class="col-md-10"><b>Batch 2014-15</b> | <b>Batch 2013-14</b> | <b>Batch 2012-13</b> | <b>Batch 2011-12</b> | <b>Batch 2010-11</b> | <b>Batch 2009-10</b> | <b>Batch 2008-09</b> | <b>Batch 2007-08</b> | </div>
				</div>
				&nbsp;
				<div class="col-md-12">
        			<table border="1" width="100%">
						<tr>
							<td align="center"><b>Batch</b></td>
							<td align="center"><b>Name</b></td>
							<td align="center"><b>Educational Qualification/Institute</b></td>
							<td align="center"><b>Designation/Organization where working</b></td>
						</tr>
						<?php foreach($alumni_result as $alumni){?>
						<tr>
							
							<td align="center"><?php echo $alumni->batch;?></td>
							<td align="center"><?php echo $alumni->name;?></td>
							<td align="center"></td>
							<td align="center"><?php echo $alumni->present_status;?></td>
							
						</tr>
						<?php } ?>
					</table>
				</div>
			</div>
		</div>
	</section>