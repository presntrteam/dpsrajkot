<?php
/* Template Name: Alumni Form */
get_header();
//$page_id = '209';
?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/js/parsleyjs/src/parsley.css">
<?php 
	
	if(!empty($_POST)){
		global $wpdb;
		$name = $_POST['names'];
		$batch = $_POST['batch'];
		$contact_number = $_POST['contact_number'];
		$email = $_POST['email'];
		$present_status = $_POST['present_status'];
		$experience_at_dps = $_POST['experience_at_dps'];
		
		$tablename='wp_alumni_form';
		$data = array('name' => $name,'batch' => $batch,'contact_number' => $contact_number,'email' => $email,'present_status' => $present_status,'experience_at_dps' => $experience_at_dps);
		
		$success=$wpdb->insert( $tablename, $data);
	}
?>
<div class="clearfix"></div>
	<section class="section">
    	<div class="container">
        	<div class="row">
            	<div class="col-md-12">

        			<div class="title-big"><h2><?php the_title(); ?></h2></div>

					<form class="form" name="alumni_form" method="POST" action="<?php echo esc_url( home_url( '/alumni-form' ) ); ?>" data-parsley-validate novalidate>
						<table width="700" border="0" cellspacing="0" cellpadding="0" class="border_01" align="center">
				          	<tbody>
				          		  <tr>
				                    <td><b class="font-style">Name</b></td>
				                    <td><input type="text" id="names" name="names" class="triple-length" required></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Batch</b></td>
				                    <td><input type="text" id="" name="batch" class="triple-length datepicker" required></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Contact Number</b></td>
				                    <td><input type="number" id="" name="contact_number" class="triple-length"></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Email</b></td>
				                    <td><input type="email" id="" name="email" class="triple-length" required></td>
				                  </tr>
				                  <tr>
				                    <td><b class="font-style">Present Status Studying /</br> Working With Details And Date</b></td>
				                    <td><textarea id="present_status" name="present_status" class="triple-length doubleheight"></textarea></td>
				                  </tr>
								   <tr>
				                    <td><b class="font-style">Your Experience AT DPS Rajkot</b></td>
				                    <td><textarea id="experience_at_dps" name="experience_at_dps" class="triple-length doubleheight" required></textarea></td>
				                  </tr>
				                  <tr>
				                    <td>&nbsp;</td>
				                    <td><input type="submit" name="Submit" value="Submit" class="button">
				                        <input type="reset" name="Reswe" value="Reset" class="button">
				                    </td>
				                  </tr>
				                  
				            </tbody>
				        </table>
					</form>
				
				</div>
			</div>
		</div>
	</section>
	<script src="<?php echo get_template_directory_uri();?>/js/parsleyjs/dist/parsley.min.js"></script>
	<script>
		(function(jQuery){
			jQuery(function(){
			jQuery('form').parsley();
			});
		})(jQuery);
	</script>