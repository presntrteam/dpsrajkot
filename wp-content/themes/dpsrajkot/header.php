<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<link href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" rel="shortcut icon">

<!-- Bootstrap -->
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css" rel="stylesheet">

<!-- Font Awesome -->
<link href="<?php echo get_template_directory_uri(); ?>/fonts/font-awesome.min.css" rel="stylesheet">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700" rel="stylesheet">

<!-- Animation CSS -->
<link href="<?php echo get_template_directory_uri(); ?>/css/animate.min.css" rel="stylesheet">

<!-- Owl Carousel -->
<link href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.css" rel="stylesheet">

<!-- Custom CSS -->
<?php /*<link href="<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet">*/ ?>
<link href="<?php echo get_template_directory_uri(); ?>/css/responsive.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/css/menu.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!--Windows Phone Js Code Start-->
<script type="text/javascript">
    // Copyright 2014-2015 Twitter, Inc.
    // Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
    if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
      var msViewportStyle = document.createElement('style')
      msViewportStyle.appendChild(
        document.createTextNode(
          '@-ms-viewport{width:auto!important}'
        )
      )
      document.querySelector('head').appendChild(msViewportStyle)
    }
</script>
<!--End Windows Phone Js Code Start-->

<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
</head>


<body>
<div class="main_wapper">
    <!--Main Header-->
    <header id="header">    
        <div class="header_wapper">
            <div class="navbar navbar-inverse">
                <div class="container">
                	<div class="topbar">
                        <?php wp_nav_menu( array( 'theme_location' => 'topmenu', 'container' => 'ul', 'menu_class' => '' ) ); ?>
                    </div>
                    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt=""></a>
                </div>
            </div>
            <div class="navigation">
                <div class="container">
                    <nav id="cssmenu">
                        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => 'ul', 'menu_class' => '' ) ); ?>
                    </nav>
                </div>
            </div>
        </div>
    </header>     
    <!--End Main Header-->
